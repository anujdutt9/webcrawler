# Web Crawler
from urllib.request import urlopen                  # Allows to connect to web pages using Python
from link_finder import LinkFinder
from general import *
from domain import *

class WebCrawler:
    # Declaring class variables so that they are visible to all web crawlers/spiders; common for all web crawlers
    # Class Variables, shared among all instances
    project_name = ''
    base_url = ''
    domain_name = ''            # To make sure we are connecting to a valid URL

    # Use text file to store urls when you switch off pc, or put it to sleep etc.
    # Stored on Hard Drive
    queue_file = ''
    crawled_file = ''

    # Use sets to store the url's when working
    # Stored on RAM; Much Faster
    queue = set()               # Waiting List
    crawled = set()             # All web pages crawled

    # Initial Code; Always runs at code startup; Initializing all values that do not change
    def __init__(self, project_name, base_url, domain_name):
        WebCrawler.project_name = project_name
        WebCrawler.base_url = base_url
        WebCrawler.domain_name = domain_name
        WebCrawler.queue_file = WebCrawler.project_name + '/queue.txt'
        WebCrawler.crawled_file = WebCrawler.project_name + '/crawled.txt'
        self.boot()

        # Intially, there is only one url. So we require only one crawler rather than many
        self.crawl_page('First WebCrawler', WebCrawler.base_url)         # (Name of WebCrawler, What page it is crawling: Initial base url page that user inputs)


    # Boot Function called at startup
    # The first webcrawler has a special Job: a) to create the new directory; b) to create new files for data;
    # After that rest of the crawlers can just access the data as it is.
    @staticmethod
    def boot():
        create_project_dir(WebCrawler.project_name)
        create_data_files(WebCrawler.project_name, WebCrawler.base_url)

        # Web Crawlers takes url from Queue (waiting list) and takes them as a "set"
        WebCrawler.queue = file_to_set(WebCrawler.queue_file)
        WebCrawler.crawled = file_to_set(WebCrawler.crawled_file)


    # Takes base url as input
    # Crawls through all URL's in HTML file
    # Saves url's in waiting list (queue)
    # Checks if that URL has already been visited before!!
    # Add the current URL to Crawled_file to avoid re-visiting the same URL
    @staticmethod
    def crawl_page(thread_name, page_url):
        if page_url not in WebCrawler.crawled:                  # Using URL from set to compare for faster access
            print(thread_name + ' is now crawling ' + page_url)
            print('URLs in Waiting List [Queue]: ' + str(len(WebCrawler.queue)))
            print('Crawled: ' + str(len(WebCrawler.crawled)) + ' URLs\n')

            # add_links_to_queue: Waiting list [queue] that all web crawlers can see and synchronize with.
            # gather_links: Makes a set of all links from all the HTML files on a web page.
            WebCrawler.add_links_to_queue(WebCrawler.gather_links(page_url))

            # Remove the current page url from queue list and put into crawled list.
            WebCrawler.queue.remove(page_url)
            WebCrawler.crawled.add(page_url)
            WebCrawler.update_files()               # Calls sets to file function and converts.



    # Python returns HTML file as bytes.
    # This function Convert bytes to Human Readable characters.
    # Returns set of links using Link Finder Class functions
    @staticmethod
    def gather_links(page_url):
        html_string = ''                            # Variable to store string parsed from HTML file
        try:
            response = urlopen(page_url)
            if 'text/html' in response.getheader('Content-type'):           # To make sure we are connecting to a HTML/PDF/txt link
                html_bytes = response.read()                                # Reads in raw data
                html_string = html_bytes.decode('utf-8')                    # Decode the raw bytes into string using utf-8 format
            finder = LinkFinder(WebCrawler.base_url, page_url)
            finder.feed(html_string)
        except:
            print('Error: Unable to Crawl Page')
            return set()
        return finder.page_links()

    # Add the gathered links to queue
    # Takes the gathered links and adds to Waiting List [Queue]
    # links is a set of links
    @staticmethod
    def add_links_to_queue(links):
        for url in links:
            # If url already exists in Queue or crawled list, it has been crawled before, so leave it.
            if (url in WebCrawler.queue) or (url in WebCrawler.crawled):
                continue
            if WebCrawler.domain_name != get_domain_name(url):
                continue
            WebCrawler.queue.add(url)

    # Update the new data from sets to a text file
    @staticmethod
    def update_files():
        set_to_file(WebCrawler.queue, WebCrawler.queue_file)
        set_to_file(WebCrawler.crawled, WebCrawler.crawled_file)

#--------------------------- EOC ------------------------
