# Goes to HTML file of url that we provide and go through all links available in the HTML file of that page.

from html.parser import HTMLParser
from urllib import parse

# INHERITANCE:
# Inherit all methods from HTMLParser and add own additional functionality to it.
# Parse all links from the HTML file.
class LinkFinder(HTMLParser):

    def error(self, message):
        pass

    def __init__(self, base_url, page_url):
        super().__init__()
        self.base_url = base_url
        self.page_url = page_url
        self.links = set()

    def handle_starttag(self, tag, attrs):          # tag: HTML tags. ex. <a></a> ; attrs: attributes. ex. <href [attribute] = 'www.an.com' [value]>
        if tag == 'a':                              # If we encounter a url (anchor -> a)
            for (attributes, values) in attrs:
                if attributes == 'href':
                    url = parse.urljoin(self.base_url, values)      # If complete url is given, its fine; if not, value joins to base url to make the link
                    self.links.add(url)                             # Add properly formatted url to a set of links

    def page_links(self):
        return self.links

#------------------------- EOC ------------------------
