# Web Crawler using Python 3
# Enter a name and url and it creates folders with filenames provided and
# populates them with two folders: queue and crawled
# Queue.txt contains urls that are in queue to be crawled [Contains all urls that are in queue to be crawled]
# Crawled.txt contains urls that have been crawled; [contains only the main url that we provide]

import os

# Creates a new folder if it does not exsists.
def create_project_dir(directory):
    if not os.path.exists(directory):
        print('Creating Project Folder...'+ directory)
        os.makedirs(directory)

#create_project_dir('WebCrawler')

# Create Queue and Crawled Files (if not created)
# Each new directory contains two files: queue and crawled files
# queue: contains list of all the links on a page waiting to be crawled
# crawled files: keeps track of each starting website that it has crawled to avoid multiple instances
# Two things required to fetch data: Project Name, Base url
def create_data_files(project_name, base_url):
    queue = os.path.join(project_name, 'queue.txt')
    crawled = os.path.join(project_name, 'crawled.txt')
    #queue = project_name + '/queue.txt'                  # File Path of queue.txt
    #crawled = project_name + '/crawled.txt'              # File Path of crawled.txt

    # if file does not exist, create it
    if not os.path.isfile(queue):
        write_file(queue, base_url)

    # if file does not exist, create it
    if not os.path.isfile(crawled):
        write_file(crawled, '')

# Write data to files; Path is name of Directory in which you want the text files to be made.
def write_file(path, data):
    with open(path, 'w') as f:
        f.write(data)

#create_data_files('WebCrawler', 'https://www.tensorflow.org/')

# Add data to existing file
def append_to_file(path, data):
    with open(path,'a') as f:
        f.write(data + '\n')

# Delete contents of a file
def delete_file_Contents(path):
    open(path,'w').close()

# Read a file and convert each line to set of items
# Using set gives us only one instance of a text and avoids repetition
# Thereby increasing speed if the url is already present
def file_to_set(file_name):
    results = set()
    with open(file_name,'rt') as f:                 # Open text document as read text
        for line in f:                              # Read text file contents line by line
            results.add(line.replace('\n',''))      # add text read to a set; remove \n added to each url
    return results

#abc = file_to_set('WebCrawler/queue.txt')
#print('abc: ',abc)

# Iterate through a set, each item will be new line in file
def set_to_file(links, file_name):
    with open(file_name,'w') as f:
        for l in sorted(links):
            f.write(l + '\n')

#----------------------- EOC -----------------------
