# Domain Name Parsing
from urllib.parse import urlparse

# Get the domain name. ex. example.com
def get_domain_name(url):
    try:
        results = get_sub_domain_name(url).split('.')
        return results[-2] + '.' + results[-1]
    except:
        return ''

# Get sub domain name. ex. name.example.com
def get_sub_domain_name(url):
    try:
        return urlparse(url).netloc                 # Parse URL with Network Location
    except:
        return ''

#print(get_domain_name('https://www.tensorflow.org/tutorials/'))
#----------------------- EOC ------------------------
