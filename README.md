# WebCrawler

**WebCrawler** is a Internet bot for browsing the World Wide Web for the purpose of Web indexing (web spidering).

Code written in Python 3.5 using PyCharm.

# Resources

| S.No.  |                       Papers / Authors                    |                     Paper Links                      |
| ------ | --------------------------------------------------------- | ---------------------------------------------------- |
|1.      | Net Instruction's Blog Post | http://www.netinstructions.com/how-to-make-a-web-crawler-in-under-50-lines-of-python-code/ |
|2.      | Stanford's Tutorial on "Web Scraping" | http://web.stanford.edu/~zlotnick/TextAsData/Web_Scraping_with_Beautiful_Soup.html |
|3.      | Web Crawler Tutorials | https://www.youtube.com/watch?v=nRW90GASSXE&list=PL6gx4Cwl9DGA8Vys-f48mAH9OKSUyav0q |
|4.      | Dataquest Web Scraping Tutorials | https://www.dataquest.io/blog/web-scraping-tutorial-python/ |
