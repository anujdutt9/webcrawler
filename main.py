import threading
from queue import Queue
from webCrawler import WebCrawler
from domain import *
from general import *

# Enter Project Name. ex. "tensorflow" for "https://www.tensorflow.org/"
#print('Enter Project Name:')
#PROJECT_NAME = input()
#print(PROJECT_NAME)
PROJECT_NAME = 'tensorflow'
HOMEPAGE = 'https://www.tensorflow.org/'
DOMAIN_NAME = get_domain_name(HOMEPAGE)
QUEUE_FILE = PROJECT_NAME + '/queue.txt'
CRAWLED_FILE = PROJECT_NAME + '/crawled.txt'
NUMBER_OF_THREADS = 9
queue = Queue()
WebCrawler(PROJECT_NAME, HOMEPAGE, DOMAIN_NAME)

# Create worker threads
# Threads die when code exits main
def create_threads():
    for _ in range(NUMBER_OF_THREADS):
        worker = threading.Thread(target=work)
        worker.daemon = True                            # So that thread dies after its done
        worker.start()

# Do the next job in the Queue
def work():
    while True:
        url = queue.get()
        WebCrawler.crawl_page(threading.current_thread().name, url)
        queue.task_done()

# Create Jobs
# Each queued link is a new job
def create_jobs():
    for link in file_to_set(QUEUE_FILE):
        queue.put(link)
    queue.join()
    crawl()

# Check if there are URLs to crawl in Queue/Wait list and crawl them
def crawl():
    queued_links = file_to_set(QUEUE_FILE)
    if len(queued_links) > 0:
        print('URLs left in Waiting List [Queue]: ',str(len(queued_links)))
        create_jobs()

create_threads()
crawl()

#---------------------------- EOC -----------------------------
